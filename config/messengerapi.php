<?php

return [
    'host' => env('MESSENGER_SERVICE_HOST', 'localhost'),
    'token' => env('MESSENGER_SERVICE_TOKEN', 'test'),
];