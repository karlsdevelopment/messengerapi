<?php

namespace Karls\MessengerApi\Exceptions;


use Karls\ErrorHandling\Exceptions\ErrorCode as CoreErrorCode;

class ErrorCode extends CoreErrorCode
{
    public const MESSENGER_SERVICE_ERROR = 50000;
}
