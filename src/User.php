<?php

namespace Karls\MessengerApi;

use Karls\ApiCore\ModelNoDb;

class User extends ModelNoDb
{
    public string $id;
    public bool $active;
}
