<?php

namespace Karls\MessengerApi;

use Illuminate\Support\Collection;
use Karls\ApiCore\ModelNoDb;

class Chat extends ModelNoDb
{
    public string $id;
    public string $type;
    public ?string $title;
    public string $lastActivity;
    public ?Collection $users;
    public ?Collection $messages;
    public ?Message $latestMessage;
}
