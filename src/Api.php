<?php

namespace Karls\MessengerApi;

use Exception;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Karls\ApiCore\BasicApi;
use Karls\ErrorHandling\Exceptions\CoreException;
use Karls\MessengerApi\Exceptions\ErrorCode;

/**
 * @method static post(string $url, array $data = null, array $files = null, array $options = null)
 * @method static get(string $url, array $query = null, array $options = null)
 * @method static put(string $url, array $data = null, array $options = null)
 * @method static mergeMessages(Collection|Message $create)
 * @method static setChat(Chat $chat)
 * @method static mergeChats($withLatestMessage)
 */
class Api extends BasicApi
{
    protected const V_PREFIX = '/api/v1';
    public const DEFAULT_AMOUNT = 25;

    public static function host(): string
    {
        return Config::get('messengerapi.host');
    }

    protected static function token(): string
    {
        return Config::get('messengerapi.token');
    }

    protected static function prepareRequest(PendingRequest $r, array $params): array
    {
        return [$r->withToken(self::token()), $params];
    }

    protected static function errorCodes(string $name): int
    {
        return match ($name) {
            'service' => ErrorCode::MESSENGER_SERVICE_ERROR,
            'default' => 0,
        };
    }

    public static function createMessage(
        string $chatId,
        string|MessengerUserInterface $sender,
        string $message = null,
        string $assetId = null)
    {
        $sender instanceof MessengerUserInterface
        && $sender = $sender->messengerUser->messengerId;

        return self::post('message/create',
            [
                'chatId' => $chatId,
                'userId' => $sender,
            ]
            + (is_null($message) ? [] : ['message' => $message])
            + (is_null($assetId) ? [] : ['assetId' => $assetId])
        );
    }

    public static function auth(array $params)
    {
        return self::get('/broadcasting/auth', $params, options: ['prefix' => '']);
    }

    public function createGroupChat(string $title, Collection $users): array
    {
        $users = collect($users)->pluck('messengerUser.messengerId')->toArray();
        return $this->post('chat/group/create', [
            'title' => $title,
            'user' => $users
        ])['data'];
    }

    /**
     * @throws CoreException
     */
    public static function createSingleChat(array $users)
    {
        try {
            $users = collect($users)->pluck('messengerUser.messengerId')->toArray();
            return self::post('chat/single/create', [
                'user' => $users
            ])['data'];
        } catch (CoreException $e) { // thrown in request()
            return $e->getDetails()['chat'] ?? throw $e;
        }
    }

    public function createUser(int $amount = 1): array
    {
        return $this->post('user/create', [
            'amount' => $amount
        ])['data'];
    }

    public function getChats(
        Collection $chatIds,
        ?string $offsetTime = null,
        ?string $chatId = null,
        int $amount = self::DEFAULT_AMOUNT,
        ?string $userId = null): array
    {
        $params = [
            'ids' => $chatIds->toArray(),
            'amount' => $amount
        ];

        $offsetTime !== null && $params['offsetTime'] = $offsetTime;
        $chatId !== null && $params['chatId'] = $chatId;
        $userId !== null && $params['userId'] = $userId;

        return $chatIds->count()
            ? $this->post('chats/get/multiple', $params)['data'] : [];
    }

    public function getChat(string $chatId)
    {
        return $this->get('chat/' . $chatId)['data'];
    }

    public static function unread(?MessengerUserInterface $user = null): array
    {
        $user = $user ?? self::loggedUser();
        return self::get('user/' . $user->messengerUser->messengerId . '/unread');
    }

    public static function setLastActivity(
        string $chatId,
        string $timestamp,
        null|MessengerUserInterface|Collection $users = null
    ): array
    {
        !$users instanceof Collection
        && $users = collect([$users ?? self::loggedUser()]);
        $messengerIds = $users->map(fn($u) => $u->messengerUser->messengerId);

        $params = [
            'chatId' => $chatId,
            'lastActivity' => $timestamp,
            'userIds' => $messengerIds->toArray(),
        ];

        return self::post('chat/user/lastActivity', $params);
    }

    public function getMessages(string $chatId, ?string $offsetTime): array
    {
        $parameter = $offsetTime === null ? [] : ['offsetTime' => $offsetTime];
        return $this->post('chat/' . $chatId . '/messages', $parameter)['data'] ?? [];
    }

    public function createAsset(string $filepath)
    {
        return $this->post('asset/create', files: ['file' => $filepath])['data'];
    }

    public function addUserToChats(MessengerUserInterface $user, array $chatIds)
    {
        return $this->post('user/' . $user->messengerUser->messengerId . '/chats/add', [
            'chatIds' => $chatIds
        ]);
    }

    public function setPushToken(MessengerUserInterface $user, string $pushToken)
    {
        return $this->post('user/' . $user->messengerUser->messengerId . '/pushToken', [
            'pushToken' => $pushToken
        ]);
    }

    /**
     * @throws Exception
     */
    private static function loggedUser()
    {
        $user = Auth::user();
        return $user instanceof MessengerUserInterface
            ? $user
            : throw new Exception('User has no Messenger association.');
    }
}
