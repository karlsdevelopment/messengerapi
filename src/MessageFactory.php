<?php

namespace Karls\MessengerApi;

use Illuminate\Support\Collection;
use Karls\ApiCore\FactoryNoDb;

class MessageFactory extends FactoryNoDb
{
    public function definition(): array
    {
        return [
            'id' => fn() => $this->faker->uuid(),
            'message' => fn() => $this->faker->sentences(3, true),
            'createdAt' => fn() => $this->faker
                ->dateTimeBetween('-1 year')
                ->format('Y-m-d H:i:s'),
            'updatedAt' => null,
            'hidden' => false,
            'asset' => null,
        ];
    }

    public function withUser(User|Collection $users): self
    {
        $users instanceof User && $users = collect([$users]);
        return $this->state([
            'user' => fn($i) => $users[$i % $users->count()],
            'userId' => fn($i) => $users[$i % $users->count()]->id,
        ]);
    }
}
