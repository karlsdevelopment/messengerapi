<?php

namespace Karls\MessengerApi;

use Illuminate\Support\ServiceProvider;

class MessengerApiServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/messengerapi.php' => config_path('messengerapi.php'),
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/messengerapi.php', 'messengerapi'
        );
        parent::register();
    }
}