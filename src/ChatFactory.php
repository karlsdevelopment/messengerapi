<?php

namespace Karls\MessengerApi;

use Illuminate\Support\Collection;
use Karls\ApiCore\FactoryNoDb;

class ChatFactory extends FactoryNoDb
{
    public const DATEFORMAT = 'Y-m-d H:i:s';
    public function definition(): array
    {
        return [
            'id' => $this->faker->uuid(),
            'type' => 'group',
            'title' => null,
            'lastActivity' => $this->faker
                ->dateTimeBetween('-1 year')
                ->format(static::DATEFORMAT),
            'latestMessage' => null,
        ];
    }

    public function withUsers(Collection $users): self
    {
        $users[0] instanceof User && $users = collect([$users]);
        return $this->state([
            'users' => fn($i) => $users[$i % $users->count()],
        ]);
    }

    public function withMessages(Collection $messages): self
    {
        $messages[0] instanceof Message && $messages = collect([$messages]);
        $cnt = $messages->count();
        return $this->state([
            'lastActivity' => fn($i) => $messages[$i % $cnt]->max('createdAt'),
            'messages' => fn($i) => $messages[$i % $cnt],
            'latestMessage' => fn($i) => $messages[$i % $cnt]->first(
                fn($m) => $m->createdAt = $messages[$i % $cnt]->max('createdAt')
            )
        ]);
    }

    public function withLatestMessage(Collection|Message $message): self
    {
        $message instanceof Message && $message = collect([$message]);
        $cnt = $message->count();
        return $this->state([
            'lastActivity' => fn($i) => $message[$i % $cnt]->createdAt,
            'latestMessage' => fn($i) => $message[$i % $cnt],
        ]);
    }
}
