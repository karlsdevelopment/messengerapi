<?php

namespace Karls\MessengerApi;

use Karls\ApiCore\FactoryNoDb;

class UserFactory extends FactoryNoDb
{
    public function definition(): array
    {
        return [
            'id' => $this->faker->uuid(),
            'active' => true,
        ];
    }
}
