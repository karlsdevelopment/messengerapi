<?php

namespace Karls\MessengerApi;

use Karls\ApiCore\ModelNoDb;

class Message extends ModelNoDb
{
    public string $id;
    public string $message;
    public string $createdAt;
    public ?string $updatedAt;
    public string $userId;
    public ?string $asset;
    public ?User $user;
}
