<?php

namespace Karls\MessengerApi;

use Faker\Provider\Uuid;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\Factory;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Karls\ApiCore\BasicApiFactory;

class ApiFactory extends BasicApiFactory
{
    public Collection $messages;
    public Chat $chat;
    public Collection $chats;
    public Collection $users;
    public const PAGE_SIZE = 25;

    public function __construct()
    {
        $this->messages = new Collection();
        $this->chats = new Collection();
        $this->users = new Collection();
        $this->chat = Chat::factory()->create();
        parent::__construct();
    }

    public function mergeMessages(Collection $messages): void
    {
        $this->messages = $this->messages->merge($messages);
    }

    public function mergeChats(Collection $chats): void
    {
        $this->chats = $this->chats->merge($chats);
    }

    public function setChat(Chat $chat): void
    {
        $this->chat = $chat;
    }

    public function fake(?array $callback = null): Factory
    {
        return Api::refreshFake(($callback ?? []) + [
                'message/create' => Http::response(['data' => []]),
                'asset/create' => fn() => self::createAsset(),
                'user/create' => fn() => $this->createUser(),
                'user/*/unread' => static::userUnread(),
                'user/*/pushToken' => fn(Request $r) => $this->setPushToken($r),
                'chat/user/lastActivity' => Http::response(['data' => []]),
                'chat/single/create' => fn() => static::createSingleChat(),
                'chat/*/messages' => fn(Request $r) => $this->chatMessages(),
                'chat/*' => fn(Request $r) => $this->chat($r),
                'chats/get/multiple' => fn(Request $r) => $this->multipleChats($r),
            ]);
    }

    private function setPushToken(Request $r): PromiseInterface
    {
        $uid = Str::beforeLast(Str::afterLast($r->url(), 'user/'), '/pushToken');
        return is_string($r['pushToken'] ?? null)
        && $this->users->where('id', $uid)->isNotEmpty()
            ? Http::response()
            : Http::response(status: 400);
    }

    private static function createAsset(): PromiseInterface
    {
        return Http::response([
            'data' => [
                'id' => Uuid::uuid(),
                'source' => Str::random(),
            ]
        ]);
    }

    private function createUser(): PromiseInterface
    {
        $this->users->add($user = ['id' => Uuid::uuid()]);
        return Http::response(['data' => [$user]]);
    }

    private static function userUnread(): PromiseInterface
    {
        return Http::response([
            'data' => [
                'unreadMessages' => 5,
                'unreadChats' => 1
            ]
        ]);
    }

    private static function createSingleChat(): PromiseInterface
    {
        return Http::response(['data' => ['id' => Uuid::uuid()]]);
    }

    private function chatMessages(): PromiseInterface
    {
        return Http::response(['data' => $this->messages->toArray()]);
    }

    private function chat(Request $r): PromiseInterface
    {
        $chatId = rtrim(Str::afterLast($r->toPsrRequest()->getUri()->getPath(), 'chat/'), '/');
        return $this->chat->id === $chatId
            ? Http::response(['data' => $this->chat])
            : Http::response([], 400);
    }

    private function multipleChats(Request $r): PromiseInterface
    {
        $chats = $this->chats
            ->where('lastActivity',
                '>',
                ($r->data()['offsetTime'] ?? Carbon::parse(0)->format('Y-m-d H:i'))
            )->where('id', '!=', $r->data()['chatId'] ?? '')
            ->sortBy(['lastActivity', 'chatId'], SORT_NATURAL)
            ->take(self::PAGE_SIZE);

        return Http::response(['data' => $chats->toArray()]);
    }
}
